package br.com.eba.ebjur.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.com.eba.ebjur.controller.HomeController;

@Configuration
@ComponentScan(basePackageClasses = HomeController.class)
public class ServiceConfig {

}
